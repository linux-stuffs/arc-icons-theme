# Arc Icon Theme

HomePage: [https://github.com/horst3180/arc-icon-theme).


## INSTALLATION

### From the binary package (Debian-based Linux distributions):

Download the latest sardi-icons package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./arc-icons-theme*.deb
```

### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/arc-icons-theme*.deb
```
